CXX =g++
CFLAGS=-g -Wall -c
EJECUTABLE=practica_03
OBJS=imple_utils.o main.o

output: $(OBJS)
	$(CXX)  $(OBJS) -o $(EJECUTABLE) && rm $(OBJS)
	
imple_utils.o: imple_utils.cpp 
	$(CXX) $(CFLAGS) imple_utils.cpp

main.o: main.cpp 
	$(CXX) $(CFLAGS) main.cpp

clean:
	rm $(EJECUTABLE)
