#!/bin/bash

git_installer () {
	git clone  git://github.com/jiangmiao/auto-pairs.git ~/.vim/bundle/auto-pairs #the auto pairs
	git clone  --recursive https://github.com/davidhalter/jedi-vim.git ~/.vim/bundle/jedi-vim #jedi vim
	git clone  https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree # nerdtree
	git clone  https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline # vim airline
	git clone  https://github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes #THEMES FOr airline
	git clone  https://github.com/tpope/vim-fugitive.git ~/.vim/bundle/vim-fugitive #fugitive
	git clone  https://github.com/w0rp/ale ~/.vim/bundle/ale #ale
	git clone https://github.com/airblade/vim-gitgutter ~/.vim/bundle/vim-gitgutter # vim-gitgutter 
}
#instaling pathogen
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

#call the git install for all the plugins 
git_installer

#install the colorscheme
cp thinkpad.vim ~/.vim/colors/
#install the snippets
cp -R .snippets/ ~/.vim/
#install the vimrc file
cp vimrc ~/.vim/

