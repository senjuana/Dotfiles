## Aliases for commands. The keys of the given dictionary are the
## aliases, while the values are the commands they map to.
## Type: Dict
c.aliases = {'w': 'session-save', 'q': 'quit', 'wq': 'quit --save'}

## Always restore open sites when qutebrowser is reopened.
## Type: Bool
c.auto_save.session = False

## Whether host blocking is enabled.
## Type: Bool
c.content.host_blocking.enabled = True

## List of URLs of lists which contain hosts to block.  The file can be
## in one of the following formats:  - An `/etc/hosts`-like file - One
## host per line - A zip-file of any of the above, with either only one
## file, or a file named   `hosts` (with any extension).
## Type: List of Url
c.content.host_blocking.lists = ['https://www.malwaredomainlist.com/hostslist/hosts.txt', 'http://someonewhocares.org/hosts/hosts', 'http://winhelp2002.mvps.org/hosts.zip', 'http://malwaredomains.lehigh.edu/files/justdomains.zip', 'https://pgl.yoyo.org/adservers/serverlist.php?hostformat=hosts&mimetype=plaintext']

## Allow websites to show notifications.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
c.content.notifications = False

## Validate SSL handshakes.
## Type: BoolAsk
## Valid values:
##   - true
##   - false
##   - ask
c.content.ssl_strict = 'ask'

## The directory to save downloads to. If unset, a sensible os-specific
## default is used.
## Type: Directory
c.downloads.location.directory = None
## Remember the last used download directory.
## Type: Bool
c.downloads.location.remember = True

## What to display in the download filename input.
## Type: String
## Valid values:
##   - path: Show only the download path.
##   - filename: Show only download filename.
##   - both: Show download path and filename.
c.downloads.location.suggestion = 'both'

## Where to show the downloaded files.
## Type: VerticalPosition
## Valid values:
##   - top
##   - bottom
c.downloads.position = 'top'

## Number of milliseconds to wait before removing finished downloads. If
## set to -1, downloads are never removed.
## Type: Int
c.downloads.remove_finished = 5

## The editor (and arguments) to use for the `open-editor` command. `{}`
## gets replaced by the filename of the file to be edited.
## Type: ShellCommand
c.editor.command = ['gvim', '-f', '{}']

## Encoding to use for the editor.
## Type: Encoding
c.editor.encoding = 'utf-8'


## Controls when a hint can be automatically followed without pressing
## Enter.
## Type: String
## Valid values:
##   - always: Auto-follow whenever there is only a single hint on a page.
##   - unique-match: Auto-follow whenever there is a unique non-empty match in either the hint string (word mode) or filter (number mode).
##   - full-match: Follow the hint when the user typed the whole hint (letter, word or number mode) or the element's text (only in number mode).
##   - never: The user will always need to press Enter to follow a hint.
c.hints.auto_follow = 'unique-match'

# Chars used for hint strings.
## Type: UniqueCharString
c.hints.chars = 'asdfghjklñ'

## Hide unmatched hints in rapid mode.
## Type: Bool
c.hints.hide_unmatched_rapid_hints = True

## Minimum number of chars used for hint strings.
## Type: Int
c.hints.min_chars = 1

## Mode to use for hints.
## Type: String
## Valid values:
##   - number: Use numeric hints. (In this mode you can also type letters from the hinted element to filter and reduce the number of elements that are hinted.)
##   - letter: Use the chars in the `hints.chars` setting.
##   - word: Use hints words based on the html elements and the extra words.
c.hints.mode = 'letter'

## Make chars in hint strings uppercase.
## Type: Bool
c.hints.uppercase = False

## The maximum time in minutes between two history items for them to be
## considered being from the same browsing session. Items with less time
## between them are grouped when being displayed in `:history`. Use -1 to
## disable separation.
## Type: Int
c.history_gap_interval = 30

## Find text on a page case-insensitively.
## Type: String
## Valid values:
##   - always: Search case-insensitively
##   - never: Search case-sensitively
##   - smart: Search case-sensitively if there are capital chars
c.search.ignore_case = 'smart'

## Leave insert mode if a non-editable element is clicked.
## Type: Bool
c.input.insert_mode.auto_leave = True

## Time from pressing a key to seeing the keyhint dialog (ms).
## Type: Int
c.keyhint.delay = 550

## Time (in ms) to show messages in the statusbar for. Set to 0 to never
## clear messages.
## Type: Int
c.messages.timeout = 2500

## Show a filebrowser in upload/download prompts.
## Type: Bool
c.prompt.filebrowser = True


## Hide the statusbar unless a message is shown.
## Type: Bool
c.statusbar.hide = True

## The position of the status bar.
## Type: VerticalPosition
## Valid values:
##   - top
##   - bottom
c.statusbar.position = 'bottom'

## On which mouse button to close tabs.
## Type: String
## Valid values:
##   - right: Close tabs on right-click.
##   - middle: Close tabs on middle-click.
##   - none: Don't close tabs using the mouse.
c.tabs.close_mouse_button = 'middle'

## Scaling for favicons in the tab bar. The tab size is unchanged, so big
## favicons also require extra `tabs.padding`.
## Type: Float
c.tabs.favicons.scale = 1.0

## Behavior when the last tab is closed.
## Type: String
## Valid values:
##   - ignore: Don't do anything.
##   - blank: Load a blank page.
##   - startpage: Load the start page.
##   - default-page: Load the default page.
##   - close: Close the window.
c.tabs.last_close = 'startpage'

## Switch between tabs using the mouse wheel.
## Type: Bool
c.tabs.mousewheel_switching = False

## How new tabs opened from another tab are positioned.
## Type: NewTabPosition
## Valid values:
##   - prev: Before the current tab.
##   - next: After the current tab.
##   - first: At the beginning.
##   - last: At the end.
c.tabs.new_position.related = 'next'

## How new tabs which aren't opened from another tab are positioned.
## Type: NewTabPosition
## Valid values:
##   - prev: Before the current tab.
##   - next: After the current tab.
##   - first: At the beginning.
##   - last: At the end.
c.tabs.new_position.unrelated = 'last'

## The position of the tab bar.
## Type: Position
## Valid values:
##   - top
##   - bottom
##   - left
##   - right
c.tabs.position = 'top'

## Open a new window for every tab.
## Type: Bool
c.tabs.tabs_are_windows = False

## Alignment of the text inside of tabs.
## Type: TextAlignment
## Valid values:
##   - left
##   - right
##   - center
c.tabs.title.alignment = 'center'

## The format to use for the tab title. The following placeholders are
## defined:  * `{perc}`: The percentage as a string like `[10%]`. *
## `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
## the current web page * `{title_sep}`: The string ` - ` if a title is
## set, empty otherwise. * `{index}`: The index of this tab. * `{id}`:
## The internal tab ID of this tab. * `{scroll_pos}`: The page scroll
## position. * `{host}`: The host of the current web page. * `{backend}`:
## Either ''webkit'' or ''webengine'' * `{private}` : Indicates when
## private mode is enabled.
## Type: FormatString
c.tabs.title.format = '{index}: {title}'

## The page to open if :open -t/-b/-w is used without URL. Use
## `about:blank` for a blank page.
## Type: FuzzyUrl
c.url.default_page = 'https://start.duckduckgo.com/'

## Definitions of search engines which can be used via the address bar.
## Maps a searchengine name (such as `DEFAULT`, or `ddg`) to a URL with a
## `{}` placeholder. The placeholder will be replaced by the search term,
## use `{{` and `}}` for literal `{`/`}` signs. The searchengine named
## `DEFAULT` is used when `url.auto_search` is turned on and something
## else than a URL was entered to be opened. Other search engines can be
## used by prepending the search engine name to the search term, e.g.
## `:open google qutebrowser`.
## Type: Dict
c.url.searchengines = {'DEFAULT':    'https://duckduckgo.com/?q={}',
                        'yt':        'https://youtube.com/results?search_query={}',
                        '4':         'https://4chan.org/{}',
                        'r':         'https://reddit.com/r/{}',
                        'w':         'https://es.wilkipedia.org/wiki/{}',
                        'aw':        'https://wiki.archlinux.org/index.php/{}'
                        }

## The page(s) to open at the start.
## Type: List of FuzzyUrl, or FuzzyUrl
c.url.start_pages = 'https://senjuana.gitlab.io/'

## The format to use for the window title. The following placeholders are
## defined:  * `{perc}`: The percentage as a string like `[10%]`. *
## `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
## the current web page * `{title_sep}`: The string ` - ` if a title is
## set, empty otherwise. * `{id}`: The internal window ID of this window.
## * `{scroll_pos}`: The page scroll position. * `{host}`: The host of
## the current web page. * `{backend}`: Either ''webkit'' or
## ''webengine'' * `{private}` : Indicates when private mode is enabled.
## Type: FormatString
c.window.title_format = '{perc}{title}{title_sep}'


## The default zoom level.
## Type: Perc
c.zoom.default = '100%'

## The available zoom levels.
## Type: List of Perc
c.zoom.levels = ['25%', '33%', '50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%', '175%', '200%', '250%', '300%', '400%', '500%']

## Whether the zoom factor on a frame applies only to the text or to all
## content.
## Type: Bool
c.zoom.text_only = False


##<theme>
config.source('nord-qutebrowser.py')

