## Encoding to use for the editor.
## Type: Encoding
c.editor.encoding = 'utf-8'

## Controls when a hint can be automatically followed without pressing
## Enter.
## Type: String
## Valid values:
##   - always: Auto-follow whenever there is only a single hint on a page.
##   - unique-match: Auto-follow whenever there is a unique non-empty match in either the hint string (word mode) or filter (number mode).
##   - full-match: Follow the hint when the user typed the whole hint (letter, word or number mode) or the element's text (only in number mode).
##   - never: The user will always need to press Enter to follow a hint.
c.hints.auto_follow = 'unique-match'

# Chars used for hint strings.
## Type: UniqueCharString
c.hints.chars = 'asdfghjklñ'

## Hide unmatched hints in rapid mode.
## Type: Bool
c.hints.hide_unmatched_rapid_hints = True

## Hide the statusbar unless a message is shown.
## Type: Bool
c.statusbar.hide = True

## Minimum number of chars used for hint strings.
## Type: Int
c.hints.min_chars = 1

## Alignment of the text inside of tabs.
## Type: TextAlignment
## Valid values:
##   - left
##   - right
##   - center
c.tabs.title.alignment = 'center'

## Mode to use for hints.
## Type: String
## Valid values:
##   - number: Use numeric hints. (In this mode you can also type letters from the hinted element to filter and reduce the number of elements that are hinted.)
##   - letter: Use the chars in the `hints.chars` setting.
##   - word: Use hints words based on the html elements and the extra words.
c.hints.mode = 'letter'

## Make chars in hint strings uppercase.
## Type: Bool
c.hints.uppercase = False

## The format to use for the tab title. The following placeholders are
## defined:  * `{perc}`: The percentage as a string like `[10%]`. *
## `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
## the current web page * `{title_sep}`: The string ` - ` if a title is
## set, empty otherwise. * `{index}`: The index of this tab. * `{id}`:
## The internal tab ID of this tab. * `{scroll_pos}`: The page scroll
## position. * `{host}`: The host of the current web page. * `{backend}`:
## Either ''webkit'' or ''webengine'' * `{private}` : Indicates when
## private mode is enabled.
## Type: FormatString
c.tabs.title.format = '{private}{index}: {title}'

## The page to open if :open -t/-b/-w is used without URL. Use
## `about:blank` for a blank page.
## Type: FuzzyUrl
c.url.default_page = 'http://search.disroot.org/'

## Definitions of search engines which can be used via the address bar.
## Maps a searchengine name (such as `DEFAULT`, or `ddg`) to a URL with a
## `{}` placeholder. The placeholder will be replaced by the search term,
## use `{{` and `}}` for literal `{`/`}` signs. The searchengine named
## `DEFAULT` is used when `url.auto_search` is turned on and something
## else than a URL was entered to be opened. Other search engines can be
## used by prepending the search engine name to the search term, e.g.
## `:open google qutebrowser`.
## Type: Dict
c.url.searchengines = {'DEFAULT':'http://search.disroot.org/?q={}',
                        's':         'https://www.startpage.com/do/search?web&query={}',
                        'yt':        'https://youtube.com/results?search_query={}',
                        '4':         'https://4chan.org/{}',
                        'r':         'https://reddit.com/r/{}',
                        'w':         'https://es.wilkipedia.org/wiki/{}',
                        'aw':        'https://wiki.archlinux.org/index.php/{}',

                        'es':        'http://search.disroot.org/?preferences=eJx1VU2T0zAM_TXkklmGjwOnHBgYBmaYWYYtXD2KrSYijhUsp93w65G7TeOy5dC0daX3np4sNaLMPonhYAIeTYK2-QResHJMJqKwP2BsxNJdP7cv07HyELoZOmxQKs8WvH4KlSOB1qMzk587CtL8pPHO04Cm5zTgIi_efLifMGRI5TOewnAhbSMfBWMmz3Ffwp4CJTRiI3u_Zr63FkXMx_svCnKMGlDRqELMFPlxaXZxxgrmxJbHyWPCphLYoyBE2zevqtTjiA2LhVhhuBb7gH5vlJbjCIk4ZBW7CHZQUT--f1W-kdUGPf282317WPn1-8MJPhdiIWHHcTGCHm3aHMGgHCjNQHYALcDsyWNm2EfEWnifjhCxdhQ1LQMYSvrrgSEZI2wJfD2iI9BDCmDMgRxyBojoHD0PmoNMHqRXoOxPjuyYO4-1Hi81TFMh4uPs1FvTYcAI2Wtr7V06FCwddVoISCqjVmpt36kUF5nchhoWgILezXbIr45LjIshG1Uhc5yFrDGnN_1pgeDw8ZaE7aRDHBKNKGcLn1SsCI66NhVkYDvh-X_sfCAspb1-LOoRRDehXo4L-Mi_lLwUs8JfklgvccSJC_M3nGc93bx7kr2moM5Kdnoj2kNkvnSihzZCfpw9yNeTDliT3BK3ym8ptdofTOesBXq-6tU-6sAS2EJY4mHhxNLzAGGDOsvZUjEtIwcdeCxPn3pzKXtcRp2xuNQpQhCvs-Ru6V1d8KRVanSOkOvSfh8hXN2K7fLVT9pvNvL65q7f_qnxIvfclLXmf8K29v4JMJZa5EgiVrddqcFCiLWkONs0xyxPty0GiyeE3D_LDuv8OHenQNmgX799--7xamyTh3bNSBDTlJdlaYzraoenXZt33s3RPFV42ZfrKqtGTD275tv9w646Lz4dl-YMUJ227J2kRf8cJqaQevoLQfZqRg==&q={}'

                        }

## The page(s) to open at the start.
## Type: List of FuzzyUrl, or FuzzyUrl
c.url.start_pages = 'http://senjuana.tk/'

## The format to use for the window title. The following placeholders are
## defined:  * `{perc}`: The percentage as a string like `[10%]`. *
## `{perc_raw}`: The raw percentage, e.g. `10` * `{title}`: The title of
## * `{scroll_pos}`: The page scroll position. * `{host}`: The host of
## the current web page. * `{backend}`: Either ''webkit'' or
## ''webengine'' * `{private}` : Indicates when private mode is enabled.
## Type: FormatString
c.window.title_format = '{perc}{title}{title_sep}'

## The default zoom level.
## Type: Perc
c.zoom.default = '100%'

## The available zoom levels.
## Type: List of Perc
c.zoom.levels = ['25%', '33%', '50%', '67%', '75%', '90%', '100%', '110%', '125%', '150%', '175%', '200%', '250%', '300%', '400%', '500%']

##<theme>
config.source('nord-qutebrowser.py')

##<binds>
config.bind('<Ctrl-i>', 'spawn --userscript qute-keepass -p /home/senjuana/Passdb-01.kdbx',mode='insert')
