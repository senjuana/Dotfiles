#!/bin/bash

#variable de directorio
DIR=$(pwd)

# funciones
report (){
	cp  -R $HOME/Documentos/Work/latex-documents/reporte $DIR
}

document (){
	cp  -R $HOME/Documentos/Work/latex-documents/document $DIR
}

presentation (){
	cp  -R $HOME/Documentos/Work/latex-documents/slides $DIR
}

# obtener los parametros del script
while (( $# )) 
	do
		echo "Creando el template de $1"
		case $1 in 		
			"reporte")    report;exit;; 
			"documento")    document;exit;;
			"presentacion")    presentation;exit;;
		esac
		shift
	done

