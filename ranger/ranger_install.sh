#!/bin/bash
#create the default config for ranger 
ranger --copy-config=all

#ask if you want install the dependency of some commands
echo "This custom configurations need Atool, w3m and ffmpegthumbnailer as dependency"
echo "You want to install it ? y == 1/n == 2"

read inputline

case $inputline in
		1) pacman -S atool; pacman -S w3m;pacman -S ffmpegthumbnailer;;
		2) echo "maybe you have problems with your commands";;
esac


echo "Instaling the custom config"
#install the custom config
cp rc.conf rifle.conf scope.sh commands.py  ~/.config/ranger

