![Image](https://i.imgur.com/qVDqLrK.png)

# Configuraciones

Este repositorio existe para tener mis configuraciones de programas que utilizo todos los dias
y tenerlas siempre actualizadas y tener una forma de instalar las configuraciones super rapida.

Se pueden instalar de forma independiente o todas las configuraciones de golpe.

Son configuraciones como las de mi vim enviroment y qutebrowser o ranger.

**Si estas configuraciones te sirven, se libre de copiarlas**



## License
> MIT License

> Copyright (c) 2017 José Ernesto Ruiz Valdiva

> Permission is hereby granted, free of charge, to any person obtaining a copy
> of this software and associated documentation files (the "Software"), to deal
> in the Software without restriction, including without limitation the rights
> to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
> copies of the Software, and to permit persons to whom the Software is
> furnished to do so, subject to the following conditions:

> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.

> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
> FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
> AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.

