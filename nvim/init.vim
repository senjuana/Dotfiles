" ---------------------------------------------------------------
" Imports
" ---------------------------------------------------------------

call plug#begin('~/.config/nvim/bundle')

Plug 'Shougo/deoplete.nvim', {'do':':UdateRemotePlugins'}
Plug 'scrooloose/nerdtree' , { 'on': 'NERDTreeToggle' }
Plug 'zchee/deoplete-jedi'
Plug 'fishbullet/deoplete-ruby'
Plug 'tweekmonster/deoplete-clang2'
Plug 'lifepillar/vim-solarized8'
Plug 'w0rp/ale'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'jiangmiao/auto-pairs'
Plug 'donraphaco/neotex', {'for': 'tex'} 
Plug 'Yggdroot/LeaderF' 
Plug 'mattn/emmet-vim'

" this plugin add the system clipboard, and it needs xsel
Plug 'christoomey/vim-system-copy'

" snipets plugins
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'"

" grammarous papi
Plug 'rhysd/vim-grammarous'

call plug#end()

" --------------------------------------------------------------
"Basics
" --------------------------------------------------------------

filetype plugin indent on
syntax on
set number 
set tabstop=4 
set softtabstop=4
set shiftwidth=4
set colorcolumn=110 

" schema
set background=dark
colorscheme solarized8_flat

" remaps
:tnoremap <Esc> <C-\><C-n>
nnoremap tn  :tabnew<CR>

" Commenting blocks of code.
autocmd FileType c,cpp,h,hpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex,bib          let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
autocmd FileType sql			  let b:comment_leader = '-- '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR> 

" Snippets
nnoremap ,main :-1read $HOME/.config/nvim/.snippets/.main.cpp<CR>:w main.cpp<CR>5ja
nnoremap ,head :-1read $HOME/.config/nvim/.snippets/.header.hpp<CR>2ji
nnoremap ,make :-1read $HOME/.config/nvim/.snippets/.make.mk<CR>:w Makefile<CR>2jA

" spellcheckers
"f5 activa el spellchecker para ingles 
map <F5> :setlocal spell! spelllang=en_us<CR> 
"f6 activa el spellchecker para español
map <F6> :setlocal spell! spelllang=es_mx<CR>  
" cheatset de spellchecker
" ]s para moverse entre para mal escritas
" z= para que te de sugerencias de la palabra
" zg para agregar al diccionario 
" zug para quitar una palabra de l diccionario

" --------------------------------------------------------------
"Plugins 
" --------------------------------------------------------------

" deoplete configuration
let g:deoplete#enable_at_startup = 1 

" ale configuration
let g:ale_sign_error = '>>'
let g:ale_sign_warning = '--'

let g:ale_completion_enabled = 1

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap) 

"vim-cpp-enhanced-highlight
let g:cpp_class_decl_highlight = 1
let g:cpp_experimental_simple_template_highlight = 1

"auto-pairs
let g:AutoPairsFlyMode = 0
let g:AutoPairsShortcutBackInsert = '<M-b>'

"neotex
let g:neotex_enabled = 1
let g:neotex_delay = 500

command! Neobib !bibtex %:r 

"leaderF
let g:Lf_ShortcutF = '<C-P>'
let g:Lf_CommandMap = {'<C-C>': ['<Esc>', '<C-C>']}

" deoplete + neosnippet + autopairs
let g:AutoPairsMapCR=0
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
imap <expr><TAB> pumvisible() ? "\<C-n>" : neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
imap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"
inoremap <expr><CR> pumvisible() ? deoplete#mappings#close_popup() : "\<CR>"


" grammarous
nmap <buffer><C-f> <Plug>(grammarous-fixit)
nmap <buffer><C-i> <Plug>(grammarous-close-info-window)

map <buffer><C-g> :GrammarousCheck<CR>

let g:grammarous#hooks = {}
function! g:grammarous#hooks.on_check(errs) abort
    nmap <buffer><C-n> <Plug>(grammarous-move-to-next-error)
    nmap <buffer><C-p> <Plug>(grammarous-move-to-previous-error)
endfunction

function! g:grammarous#hooks.on_reset(errs) abort
    nunmap <buffer><C-n>
    nunmap <buffer><C-p>
endfunction




