#!/bin/bash

# install vim-Plug
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#install the snippets
cp -R .snippets/ ~/.config/nvim
#install the vimrc file
cp init.vim ~/.config/nvim

