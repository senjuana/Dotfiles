#!/bin/bash

#funtions for call the individual .sh
install(){
	cd $1/
	chmod +x $1_install.sh
	./$1_install.sh
	cd ..
	echo "The $1 config was installed"
	echo
}

allinstall(){
	array=('urxvt' 'bash' 'tmux' 'ranger' 'qute' 'nvim' 'latex' [8]='vim')
	for item in ${array[*]} 
	do
      install $item
	done
}

# MENU
if smenu -V; then

  MENU+="\
	'Terminal' 'Apps' 'Extención'
	'ur URXVT' 'ra Ranger' 'lat Latex'
	'ba Bash' 'qu Qutebrowser' 'vi Vim'
	'tmu Tmux' 'nv Nvim' 'za Zathura'
	#
	'All Todas'
	#
	'Quit Exit menu'"

	trap "exit 1" INT

	while true
	do
	   clear
	   echo "Executing ${0##*/}"
	   echo

	   MESSAGE="Instalación Dotfiles"
	   read REP <<< $(echo "$MENU" | ./menu.sh -i "$MESSAGE" "-Re1")

	   case $REP in
		 ABORT) exit 1         ;;
		 QUIT)  exit 0         ;;

		 All)   echo Todos los dotfiles; allinstall;;

		 ur)   echo Instalando URXVT; install "urxvt";;
		 ba)   echo Instalando Bash; install "bash";;
		 tmu)  echo Instalando tmux; install "tmux";;

		 ra)   echo Instalando ranger; install "ranger";;
		 qu)   echo Instalando qutebrowser; install "qute";;
		 nv)   echo Instalando Neovim; install "nvim";;

		 lat)  echo Instalando latex; install "latex";;
		 vi)   echo Instalando Vim; install "vim";;
		 za)   echo Instalando Zathura; install "zathura";;
	   esac

	   echo
	   echo -n "Presiona <Enter> para continuar"
	   read
	done

	exit 0
else
   echo "Necesitas instalar Smenu https://github.com/p-gen/smenu"
   echo "yaourt smenu"
fi
